const puppeteer = require('puppeteer');
var express = require('express');
var app = express()
const port = 1001;
var path = require('path');
const fs = require("fs");
const bodyParser = require('body-parser')
var moment = require('moment');
// (async () => 
// {
//     const browser = await puppeteer.launch();
//     const page = await browser.newPage();
//     const options ={
//         path : 'test.pdf',
//         format : 'A4'
//     }
//     await page.goto(path.join(__dirname) + '/test.html', {waitUntil : 'networkidle2'});
//     await page.pdf(options)
//     await browser.close();
// })();

// let config = {
//   headers: {
//     'Authorization': 'Bearer ' + req.headers.authorization.split(" ")[1]
//   }
// }

app.use(bodyParser.json({limit: '250mb', type: 'application/json'}));
app.use(bodyParser.json())

app.get('/:directory/:filename', (req, res)=>
{
    var directory = req.params.directory
    var filename = req.params.filename
    var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress || (req.socket ? req.socket.remoteAddress : null);
    var json={code: 300,status:"pending",ip:ip}
    if (ip == '127.0.0.1' || ip == '::1' || ip == '::ffff:127.0.0.1')
    {
      if (fs.existsSync(path.join(__dirname) +'/reports/' + directory))
      {
        var file = path.join(__dirname) +`/reports/${directory}/${filename}`
        fs.readFile(file, function (err, filex) 
        {
          res.writeHead(200, 
          {
            'Content-Type': 'text/plain',
            'Content-disposition': 'attachment;filename=' + filename,
            'Content-Length': filex.length
          });
          res.end(Buffer.from(filex, 'binary'));
        })
      }
      else
      {
        json.code=400
        res.send(json)
      }

    }
    else
    {
      res.send(json)
    }
})

app.post('/', async (req, res) =>
{
    const Request = req.body
    var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress || (req.socket ? req.socket.remoteAddress : null);
    var json={code: 300,status:"pending",ip:ip}
    if (ip == '127.0.0.1' || ip == '::1' || ip == '::ffff:127.0.0.1')
    {
      try {
        let myreport_md5 = Request.myreport_md5
        if (!fs.existsSync(path.join(__dirname) +'/reports/' + myreport_md5))
        {
          fs.mkdirSync(path.join(__dirname) +'/reports/' + myreport_md5, 0744);
        }
        let url = Request.url
        let uri = {}
        uri.token = req.headers.authorization.split(" ")[1];
        uri.company_id = Request.members.company_id
        uri.department_id = Request.members.depart_id
        uri.staff_id = Request.members.staff_id
        uri.parameters = Request.parameters
        let Suri = Buffer.from(JSON.stringify(uri)).toString('base64')
        url = url + Suri
        const browser = await puppeteer.launch();
        const page = await browser.newPage();

        const pdf = myreport_md5 + moment().format('YYYYMMDDHHmmss') + '.pdf'
        const options ={
                  path : path.join(__dirname) +'/reports/' + myreport_md5 + '/' + pdf,
                  displayHeaderFooter: true,
                  headerTemplate: '',
                  footerTemplate: '',
                  printBackground: true,
                  format : 'A4'
              }
        await page.goto(url, {
          timeout: 20000,
          waitUntil: ['load', 'domcontentloaded', 'networkidle0', 'networkidle2']
          });
        await page.pdf(options)
        await browser.close();
        json.file = pdf
        json.code = 200
        json.status='success'
        res.send(json)
      } catch (error) {
        console.log(error.message)
      }


    }
    else
    {
        res.send(json)
    }
})

app.listen(port, () => console.log("Puppeteer Server listening on port 1001!"))